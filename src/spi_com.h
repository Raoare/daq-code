/*
 * spi_com.h
 *
 * Created: 6/20/2019 3:33:04 PM
 *  Author: F
 */ 


#ifndef SPI_COM_H_
#define SPI_COM_H_

#include <asf.h>

/* Chip select. */
#define SPI_CHIP_SEL 0
#define SPI_CHIP_PCS spi_get_pcs(SPI_CHIP_SEL)

/* Clock polarity. */
#define SPI_CLK_POLARITY 0

/* Clock phase. */
#define SPI_CLK_PHASE 0

/* Delay before SPCK. */
#define SPI_DLYBS 0x40

/* Delay between consecutive transfers. */
#define SPI_DLYBCT 0x10

/* SPI clock setting (Hz). */
static uint32_t gs_ul_spi_clock = 20000000;

#define FB_DELAY_US 2

void configure_spi_master(void);
void txrx(uint8_t *tx, uint8_t *rx, uint32_t size, bool first_byte_delay);

#endif /* SPI_COM_H_ */
