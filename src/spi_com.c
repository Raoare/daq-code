// A delay is added to the SPI functions such that the end of the
// second byte arrives at least 2us after the start of the first
// byte per page 40 of the ADS1299 datasheet
#include "spi_com.h"

/******************************************************************
 *
 * Description: Configures the SPI
 * Last Modified: 06/26/19
 *
 ******************************************************************/
void configure_spi_master(void) {
    spi_enable_clock(SPI_MASTER_BASE);
    spi_disable(SPI_MASTER_BASE);
    spi_reset(SPI_MASTER_BASE);
    spi_set_lastxfer(SPI_MASTER_BASE);
    spi_set_master_mode(SPI_MASTER_BASE);
    spi_disable_mode_fault_detect(SPI_MASTER_BASE);
    spi_set_peripheral_chip_select_value(SPI_MASTER_BASE, SPI_CHIP_PCS);
    spi_set_clock_polarity(SPI_MASTER_BASE, SPI_CHIP_SEL, SPI_CLK_POLARITY);
    spi_set_clock_phase(SPI_MASTER_BASE, SPI_CHIP_SEL, SPI_CLK_PHASE);
    spi_set_bits_per_transfer(SPI_MASTER_BASE, SPI_CHIP_SEL, SPI_CSR_BITS_8_BIT);
    spi_set_baudrate_div(SPI_MASTER_BASE, SPI_CHIP_SEL,(sysclk_get_peripheral_hz()/gs_ul_spi_clock));
    spi_set_transfer_delay(SPI_MASTER_BASE, SPI_CHIP_SEL, SPI_DLYBS, SPI_DLYBCT);
    spi_enable(SPI_MASTER_BASE);
}

void __attribute__((optimize("O0"))) txrx(uint8_t *tx, uint8_t *rx, uint32_t size, bool first_byte_delay) {
	uint32_t i;
	uint8_t uc_pcs, temp;

	for (i = 0; i < size; i++) {
		if (SPI_OK == spi_write(SPI_MASTER_BASE, tx[i], 0, 0)) {
			while ((spi_read_status(SPI_MASTER_BASE) & SPI_SR_RDRF) == 0);
			if (rx != NULL) spi_read(SPI_MASTER_BASE, &(rx[i]), &uc_pcs);
			else spi_read(SPI_MASTER_BASE, &temp, &uc_pcs);
			if (first_byte_delay && i == 0) delay_us(FB_DELAY_US);
		}
	}
}
