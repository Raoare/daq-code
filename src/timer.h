#ifndef TIMER_H_
#define TIMER_H_

#include <stdbool.h>
#include <asf.h>
#include <math.h>

#define NUM_TIMERS 1
#define CHANNELS_PER_TIMER 3
#define TOTAL_TIMERS_AVAILABLE (NUM_TIMERS * CHANNELS_PER_TIMER)

#define TOPTS_ONCE (1 << 1)
#define TOPTS_BYTE_4 (1 << 2)
#define TOPTS_BYTE_2 (1 << 3)

#define TC_DIVISOR_1 2
#define TC_DIVISOR_2 8
#define TC_DIVISOR_3 32
#define TC_DIVISOR_4 128

enum ERROR_CODES {
	NO_ERROR,
	IN_USE,
	NOT_POSSIBLE,
	RATE_ERROR,
	CLK_FEED_ERROR
};

//TODO: delete four_B from both... use timers used, if not equal then you know its 4B
typedef struct Timer_Setup {
	bool four_B;
	bool in_use;
	uint16_t counter;
    uint32_t timer_num;
    uint32_t channel_num;
	uint32_t opts;
} TIMER;

typedef struct Timer_Usage {
	bool continuous;
	double rate;
	int timers_used[2];
	void (*fn) (void);
	struct Timer_Usage *next;
} TU;

enum ERROR_CODES timer_init();
uint32_t t_find_timer_num(uint16_t x);
uint32_t t_find_chan_num(uint16_t x);
enum ERROR_CODES t_copy_timer(TIMER x, uint16_t t_num);
uint16_t t_find_clock_divisor(double rate, bool four_bytes);
TIMER t_make_timer(double rate);
uint32_t t_clock_feeding(uint16_t from, uint16_t to);
void t_insert_timer(TU *x);
uint16_t t_counter_value_32b(uint32_t opts, uint16_t cntr, double rate);
// TODO: Force 32-bit option in the setup timer
TU* setup_timer(bool once, double rate, void (*fn) (void));
void t_enable_disable_clock(uint16_t x, bool enable, bool interrupt);
enum ERROR_CODES stop_timer(TU *t);
enum ERROR_CODES start_timer(TU *t);
enum ERROR_CODES rm_timer(TU *t);
// TODO: make a reconfigure timer function

void t_pop_timer(TU *t);
void Timer_Handler(uint16_t chan);

#endif /* TIMER_H_ */
