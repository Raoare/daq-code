#include "adcLib.h"

//Flag to determine if ADC has new data
bool dataRdy = false;
float *adcValBuffer;

/******************************************************************
 *
 * Description: Writes the register to change the ADC sample rate
 *  with the rate from the input
 * Last Modified: 11/1/17
 *
******************************************************************/
void changeSampleRate (uint8_t rate) {
    writeReg(CONFIG1_REG, (rate & 0b00000111) + CONFIG1_REG_INIT);
}

/******************************************************************
 *
 * Description: Sets which channels to 'turn on' on the ADC.  Data
 *  is sent from all channels regardless.
 * Last Modified: 11/1/17
 *
 ******************************************************************/
void change_channel (uint8_t channel) {	
	if ((1) & channel) writeReg(CH_0_SET_REG, CHSET_ON_REG_VAL);
	else writeReg(CH_0_SET_REG, CHSET_OFF_REG_VAL);
	
	if ((1 << 1) & channel) writeReg(CH_1_SET_REG, CHSET_ON_REG_VAL);
	else writeReg(CH_1_SET_REG, CHSET_OFF_REG_VAL);
	
	if ((1 << 2) & channel) writeReg(CH_2_SET_REG, CHSET_ON_REG_VAL);
	else writeReg(CH_2_SET_REG, CHSET_OFF_REG_VAL);
	
	if ((1 << 3) & channel) writeReg(CH_3_SET_REG, CHSET_ON_REG_VAL);
	else writeReg(CH_3_SET_REG, CHSET_OFF_REG_VAL);
	
	if (4 < HIGHEST_CHANNEL) {
		if ((1 << 4) & channel) writeReg(CH_4_SET_REG, CHSET_ON_REG_VAL);
		else writeReg(CH_4_SET_REG, CHSET_OFF_REG_VAL);
		
		if ((1 << 5) & channel) writeReg(CH_5_SET_REG, CHSET_ON_REG_VAL);
		else writeReg(CH_5_SET_REG, CHSET_OFF_REG_VAL);
		
		if (6 < HIGHEST_CHANNEL) {
			if ((1 << 6) & channel) writeReg(CH_6_SET_REG, CHSET_ON_REG_VAL);
			else writeReg(CH_6_SET_REG, CHSET_OFF_REG_VAL);
			
			if ((1 << 7) & channel) writeReg(CH_7_SET_REG, CHSET_ON_REG_VAL);
			else writeReg(CH_7_SET_REG, CHSET_OFF_REG_VAL);
		}
	}
}

/******************************************************************
 *
 * Description: Initializes Registers setting the sample rate and
 *  channels to record on.
 * Last Modified: 11/1/17
 *
 ******************************************************************/
void initReg(uint8_t rate, uint8_t channel) {
    changeSampleRate(rate);
    change_channel(channel);
}

float data_to_value(uint8_t *data) {
    return (float) ((((data[0] & 0x80) ? 0xff : 0) << 24) | (data[0] << 16) | (data[1] << 8) | (data[2]))*(float) (ADC_CONVERSION);
}

/******************************************************************
 *
 * Description: Reads data from the ADC and stores it in the global
 *  array 'adcData'.  Retries until either 3 attempts or expected
 *  first byte is recieved.
 * Last Modified: 11/1/17
 *
 ******************************************************************/
void readADC(void) {
    static uint8_t read_tx[22] = {READ_ADC,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    static uint8_t adcData[ADC_BYTES_PER_SAMPLE+4];
    uint8_t i;
    static float *oldPtr = NULL;
    static unsigned int numTimesAvg = 0;
    static int ktest = 0;
    
    cpu_irq_enter_critical();
    do {
        txrx(read_tx, &adcData, 22, true);
    } while (((adcData[1] & 0xF0) != 0xC0) && i++ < 3);
	
    if (oldPtr == adcValBuffer) numTimesAvg++;
    else {
        numTimesAvg = 0;
        oldPtr = adcValBuffer;
    }
    
	for (i = 0; i < ADC_NUM_CHANNELS; i++) {
        //adcValBuffer[i] = (float) numTimesAvg;
        adcValBuffer[i] = ((adcValBuffer[i] * numTimesAvg) + data_to_value(&adcData[i*ADC_BYTES_PER_SINGLE_SAMPLE+4])) / (numTimesAvg+1);
	}
    cpu_irq_leave_critical();
}

/******************************************************************
 *
 * Description: Callback function for pin on ADC saying data is ready
 * Last Modified: 11/1/17
 *
 ******************************************************************/
void drdy_callback(void) {
	readADC();
	dataRdy = true;
}

/******************************************************************
 *
 * Description: Initializes all GPIO Pins direction and state
 * Last Modified: 11/1/17
 *
 ******************************************************************/
void initGPIO(void) {
	
	// Enable peripheral clock
	pmc_enable_periph_clk(ID_PIOA);
	
	// Set IO pins
	pio_set_output(PIOA, RST_PIN, HIGH, DISABLE, ENABLE);
	pio_set_output(PIOA, PWDN_PIN, HIGH, DISABLE, ENABLE);
	pio_set_output(PIOA, START_PIN, LOW, DISABLE, ENABLE);
	pio_set_output(PIOA, CS_PIN, LOW, DISABLE, ENABLE);
	pio_set_input(PIOA, DRDY_PIN, PIO_DEGLITCH);
	
	// Configure interrupt
	pio_handler_set(PIOA, ID_PIOA, DRDY_PIN, PIO_IT_FALL_EDGE, &drdy_callback);
	NVIC_EnableIRQ(PIOA_IRQn);
	
	// Set initial states
	enableDrdy(false);
	startADC(false);
	reset_ADC();
}

/******************************************************************
 *
 * Description: Resets the ADC using the pwdn pin.  There is also a
 *  rst pin.
 * Last Modified: 11/1/17
 *
 ******************************************************************/
void reset_ADC(void) {
	uint8_t tx[2] = {STOP_CONT_ADC, STOP_ADC};
    turnOn(false);
	//Needs at least 9ms
	delay_ms(100);
	turnOn(true);
	delay_ms(100);
	txrx(tx,NULL,2,false);
}

/******************************************************************
 *
 * Description: Enables or Disables the drdy callback depending on
 *  the input
 * Last Modified: 11/1/17
 *
 ******************************************************************/
void enableDrdy(bool val) {
	val ? pio_enable_interrupt(PIOA, DRDY_PIN) : pio_disable_interrupt(PIOA, DRDY_PIN);
}

/******************************************************************
 *
 * Description: Turns on or off the ADC depending on the input
 * Last Modified: 11/1/17
 *
 ******************************************************************/
void turnOn(bool val) {
    val ? pio_set(PIOA, PWDN_PIN) : pio_clear(PIOA, PWDN_PIN);
}

/******************************************************************
 *
 * Description: Sets the start pin depending on the input
 * Last Modified: 11/1/17
 *
 ******************************************************************/
void startADC(bool val) {
    val ? pio_set(PIOA, START_PIN) : pio_clear(PIOA, START_PIN);
}

/******************************************************************
 *
 * Description: Writes the input register with the input value.
 *  ADC allows for multiple sequential registers to be written, but
 *  this functionality is ignored.
 * Last Modified: 11/1/17
 *
 ******************************************************************/
void writeReg(uint8_t reg, uint8_t value) {
    uint8_t attempts = 0, tx[3] = {(WRITE_REG + reg), 0, value};
	do {
		txrx(tx, NULL, 3, false);
    } while (value != readReg(reg) && attempts++ < 3);
}

/******************************************************************
 *
 * Description: Reads the input register and returns that value.
 *  ADC allows for multiple sequential registers to be read, but this
 *  functionality is ignored.
 * Last Modified: 11/1/17
 *
 ******************************************************************/
uint8_t readReg(uint8_t reg) {
	uint8_t tx[3] = {(READ_REG + reg), 0, 0};
	if (reg > CONFIG4_REG) return 0;
    txrx(tx, tx, 3, true);
    return tx[2];
}

/******************************************************************
 *
 * Description: Initializes the ADC.  SPI is configured, GPIO is set,
 *  Registers are set to initialization states.
 * Last Modified: 11/1/17
 *
 ******************************************************************/
void __attribute__((optimize("O0"))) initADC(void) {
	configure_spi_master();
    initGPIO();
    initReg(DATA_RATE_16000,0b00111111);
	writeReg(CONFIG1_REG,CONFIG1_REG_INIT);
	writeReg(CONFIG2_REG,CONFIG2_REG_INIT);
	writeReg(CONFIG3_REG,CONFIG3_REG_INIT);
	writeReg(MISC1_REG,MISC1_REG_INIT);
}

/******************************************************************
 *
 * Description: Determines an appropriate ADC sample rate with the
 *  users desired sample rate as the input.  'FF' is a 'fudge factor'
 *  since we need a little extra time to transfer data between samples
 * Last Modified: 11/1/17
 *
 ******************************************************************/
uint8_t determineADCRate(float rate) {
    if (rate < (float) RATE_1000 * FF) {
        if (rate < (float) RATE_500 * FF) return (rate < (float) RATE_250 * FF) ? DATA_RATE_250 : DATA_RATE_500;
        else return DATA_RATE_1000;
    }
    else {
        if (rate < (float) RATE_4000 * FF) return (rate < (float) RATE_2000 * FF) ? DATA_RATE_2000 : DATA_RATE_4000;
		else return (rate < (float) RATE_8000 * FF) ? DATA_RATE_8000 : DATA_RATE_16000;
    }
}
