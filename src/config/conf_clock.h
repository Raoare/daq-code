#ifndef CONF_CLOCK_H_INCLUDED
#define CONF_CLOCK_H_INCLUDED

// Crystal Freuqnency
#define BOARD_FREQ_MAINCK_XTAL 16000000UL
#define BOARD_OSC_STARTUP_US   15625UL

#define CONFIG_SYSCLK_SOURCE        SYSCLK_SRC_PLLACK

// ===== System Clock (MCK) Prescaler Options   (Fmck = Fsys / (SYSCLK_PRES))
#define CONFIG_SYSCLK_PRES          SYSCLK_PRES_2

// ===== PLL0 (A) Options   (Fpll = (Fclk * PLL_mul) / PLL_div)
#define CONFIG_PLL0_SOURCE          PLL_SRC_MAINCK_XTAL
#define CONFIG_PLL0_MUL             15 // (Desired Frequency (120MHz) / CLK Frequency (16MHz)) x 2
#define CONFIG_PLL0_DIV             1

// ===== PLL1 (B) Options   (Fpll = (Fclk * PLL_mul) / PLL_div)
#define CONFIG_PLL1_SOURCE          PLL_SRC_MAINCK_XTAL
#define CONFIG_PLL1_MUL             3 // Desired Frequency (48MHz) / CLK Frequency (16MHz)
#define CONFIG_PLL1_DIV             1

// ===== USB Clock Source Options   (Fusb = FpllX / USB_div)
#define CONFIG_USBCLK_SOURCE        USBCLK_SRC_PLL1
#define CONFIG_USBCLK_DIV           1

#endif /* CONF_CLOCK_H_INCLUDED */
