#include "timer.h"

TIMER T[TOTAL_TIMERS_AVAILABLE];
TU *timer = NULL;

enum ERROR_CODES timer_init() {
	uint32_t x = 0;
	do {
		T[x].in_use = false;
        T[x].timer_num = t_find_timer_num(x);
        T[x].channel_num = t_find_chan_num(x);
	} while (++x < TOTAL_TIMERS_AVAILABLE);
}

// TODO: Turn into macro
uint32_t t_find_timer_num(uint16_t x) {
#if (NUM_TIMERS == 1)
	return TC0;
#else
	return (x < CHANNELS_PER_TIMER) ? TC0 : TC1;
#endif
}

// TODO: Turn into macro
uint32_t t_find_chan_num(uint16_t x) {
	return x % CHANNELS_PER_TIMER;
}

enum ERROR_CODES t_copy_timer(TIMER x, uint16_t t_num) {
	if (t_num >= TOTAL_TIMERS_AVAILABLE) return NOT_POSSIBLE;
	
	T[t_num].counter = x.counter;
	T[t_num].four_B = x.four_B;
	T[t_num].opts = x.opts;
	
	return NO_ERROR;
};

uint16_t t_find_clock_divisor(double rate, bool four_bytes) {
	double clk_freq = (double) (sysclk_get_cpu_hz() / (uint16_t) 0xFFFF);
	
	if (four_bytes) clk_freq = (double) (clk_freq / 2 / (uint16_t) 0xFFFF);
	
	if (rate > (clk_freq / TC_DIVISOR_1)) return TC_DIVISOR_1;
	else if (rate > (clk_freq / TC_DIVISOR_2)) return TC_DIVISOR_2;
	else if (rate > (clk_freq / TC_DIVISOR_3)) return TC_DIVISOR_3;
	else if (rate > (clk_freq / TC_DIVISOR_4)) return TC_DIVISOR_4;
	else return 0;
}

TIMER t_make_timer(double rate) {
	uint16_t cd16b = t_find_clock_divisor(rate, false);
	uint16_t cd32b = t_find_clock_divisor(rate, true);
	uint16_t c_temp, cd = cd32b;
	uint32_t temp_counter;
	double t32b, t16b;
	TIMER t;
	
	// Catch initial errors
	t.in_use = true;
	if (cd == 0) return t;
	else {
		// Initialization
		temp_counter = lround(sysclk_get_cpu_hz()/cd32b/rate);
		// Initialize timer
		t.in_use = false;
		t.four_B = true;
		t.opts = 0;
	}
	
	// Use 16b or 32b timer?
	if (cd16b != 0) {
		c_temp = (uint16_t) lround(sysclk_get_cpu_hz()/cd16b/rate);
		// Determine error.  If error is equal, use 16b timer
		t32b = fabs((double) sysclk_get_cpu_hz()/cd32b/temp_counter - rate);
		t16b = fabs((double) sysclk_get_cpu_hz()/cd16b/c_temp - rate);
		if (t32b >= t16b) {
			t.counter = c_temp;
			t.four_B = false;
			cd = cd16b;
		}
		else {
			t.counter = (uint16_t) ceil((double) temp_counter / 2 / (double) 0xFFFF);
		}
	}
	else {
		t.counter = (uint16_t) ceil((double) temp_counter / 2 / (double) 0xFFFF);
	}
	
	t.opts |= TC_CMR_WAVSEL_UP_RC | TC_CMR_WAVE;
	switch (cd) {
		case TC_DIVISOR_1:
			t.opts |= TC_CMR_TCCLKS_TIMER_CLOCK1;
			break;
		case TC_DIVISOR_2:
			t.opts |= TC_CMR_TCCLKS_TIMER_CLOCK2;
			break;
		case TC_DIVISOR_3:
			t.opts |= TC_CMR_TCCLKS_TIMER_CLOCK3;
			break;
		case TC_DIVISOR_4:
			t.opts |= TC_CMR_TCCLKS_TIMER_CLOCK4;
			break;
	}
	
	t.opts |= (t.four_B) ? TC_CMR_ACPC_TOGGLE : TC_CMR_WAVE;
	
	return t;
}

uint16_t t_counter_value_32b(uint32_t opts, uint16_t cntr, double rate) {
	uint16_t divisor = TC_DIVISOR_1;
	double cnt = sysclk_get_cpu_hz()/2;
	
	if (opts & TC_CMR_TCCLKS_TIMER_CLOCK2) divisor = TC_DIVISOR_2;
	else if (opts & TC_CMR_TCCLKS_TIMER_CLOCK3) divisor = TC_DIVISOR_3;
	else if (opts & TC_CMR_TCCLKS_TIMER_CLOCK4) divisor = TC_DIVISOR_4;
	
	cnt = (double) cnt/divisor/cntr/rate;
	
	return (uint16_t) lround(cnt);
}

uint32_t t_clock_feeding(uint16_t from, uint16_t to) {
	uint32_t temp, rtn = NULL, t_to = t_find_chan_num(to);
	if ((t_find_timer_num(from) != t_find_timer_num(to)) || (from == to) || (from >= CHANNELS_PER_TIMER) || (to >= CHANNELS_PER_TIMER)) return NULL;
	
	switch(t_find_chan_num(from)) {
		case 0:
			if (1 == t_to) {
				temp = TC_BMR_TC1XC1S_TIOA0;
				rtn = TC_CMR_TCCLKS_XC1;
			}
			else if (2 == t_to) {
				temp = TC_BMR_TC2XC2S_TIOA0;
				rtn = TC_CMR_TCCLKS_XC2;
			}
			break;
		case 1:
			if (0 == t_to) {
				temp = TC_BMR_TC0XC0S_TIOA1;
				rtn = TC_CMR_TCCLKS_XC0;
			}
			else if (2 == t_to) {
				temp = TC_BMR_TC2XC2S_TIOA1;
				rtn = TC_CMR_TCCLKS_XC2;
			}
			break;
		case 2:
			if (0 == t_to) {
				temp = TC_BMR_TC0XC0S_TIOA2;
				rtn = TC_CMR_TCCLKS_XC0;
			}
			else if (1 == t_to) {
				temp = TC_BMR_TC1XC1S_TIOA2;
				rtn = TC_CMR_TCCLKS_XC1;
			}
			break;
		default:
			break;
	}
	tc_set_block_mode(t_find_timer_num(from), temp);
	
	return rtn;
}

void t_insert_timer(TU *x) {
	TU *ptr;
	
	if (timer == NULL) timer = x;
	else {
		for(ptr = timer; ptr->next != NULL; ptr = ptr->next);
		ptr->next = x;
	}
}

TU* setup_timer(bool once, double rate, void (*fn) (void)) {
	TIMER temp_t = t_make_timer(rate);
	TU *temp_tu;
	uint16_t x, y, temp_clk;
	
	// Catch initial errors here
	if (temp_t.in_use) return NULL;
	
	// Find next available timers
	for (x = 0; T[x].in_use == true && x < TOTAL_TIMERS_AVAILABLE; x++);
	
	if (x == TOTAL_TIMERS_AVAILABLE) return NULL;
	else T[x].in_use = true;
	
	for (y = x; T[y].in_use == true  && y < TOTAL_TIMERS_AVAILABLE; y++);
	
	if (temp_t.four_B) {
		if (y == TOTAL_TIMERS_AVAILABLE) {
			T[x].in_use = false;
			return NULL;
		}
		else T[y].in_use = true;
	}
	
	// Ensure we can feed a clock for a 32b timer
	if (temp_t.four_B) {
		if ((temp_clk = t_clock_feeding(x,y)) == NULL) {
			T[x].in_use = false;
			T[y].in_use = false;
			return NULL;
		}
	}
	
	t_copy_timer(temp_t, x);
	tc_init(t_find_timer_num(x),t_find_chan_num(x),T[x].opts);
	tc_write_rc(t_find_timer_num(x),t_find_chan_num(x),T[x].counter);
	
	// Save timer info
	temp_tu = malloc(sizeof(TU));
	temp_tu->continuous = !once;
	temp_tu->fn = fn;
	temp_tu->next = NULL;
	temp_tu->rate = rate;
	temp_tu->timers_used[1] = x;
	if (temp_t.four_B) temp_tu->timers_used[0] = y;
	else temp_tu->timers_used[0] = x;
	t_insert_timer(temp_tu);
	
	if (temp_t.four_B) {
		temp_t.counter = t_counter_value_32b(T[x].opts, T[x].counter, rate);
		temp_t.opts = temp_clk | TC_CMR_CPCTRG;
		t_copy_timer(temp_t, y);
		tc_init(t_find_timer_num(y),t_find_chan_num(y),T[y].opts);
		tc_write_rc(t_find_timer_num(y),t_find_chan_num(y),T[y].counter);
		t_enable_disable_clock(x,true,false);
		t_enable_disable_clock(y,true,true);
		tc_enable_interrupt(t_find_timer_num(y), t_find_chan_num(y), TC_IER_CPCS);
	}
	else {
		t_enable_disable_clock(x, true, true);
		tc_enable_interrupt(t_find_timer_num(x), t_find_chan_num(x), TC_IER_CPCS);
	}
	
	return temp_tu;
}

void t_enable_disable_clock(uint16_t x, bool enable, bool interrupt) {
	if (enable) {
		pmc_enable_periph_clk(ID_TC0 + t_find_chan_num(x));
		if (interrupt) {
			NVIC_SetPriority(TC0_IRQn + t_find_chan_num(x), 0);
			NVIC_EnableIRQ(TC0_IRQn + t_find_chan_num(x));
		}
	}
	else {
		pmc_disable_periph_clk(ID_TC0 + t_find_chan_num(x));
		if (interrupt) {
			NVIC_DisableIRQ(TC0_IRQn + t_find_chan_num(x));
			NVIC_ClearPendingIRQ(TC0_IRQn + t_find_chan_num(x));
		}
	}
}

enum ERROR_CODES stop_timer(TU *t) {
	if (t == NULL) return NOT_POSSIBLE;
	else {
		if (T[t->timers_used[0]].four_B) tc_stop(t_find_timer_num(t->timers_used[1]), t_find_chan_num(t->timers_used[1]));
		tc_stop(t_find_timer_num(t->timers_used[0]), t_find_chan_num(t->timers_used[0]));
	}
}

enum ERROR_CODES start_timer(TU *t) {
	if (t == NULL) return NOT_POSSIBLE;
	else {
		if (T[t->timers_used[0]].four_B) tc_start(t_find_timer_num(t->timers_used[1]), t_find_chan_num(t->timers_used[1]));
		tc_start(t_find_timer_num(t->timers_used[0]), t_find_chan_num(t->timers_used[0]));
	}
}

enum ERROR_CODES rm_timer(TU *t) {
	uint16_t i;
	bool rm0 = true, rm1 = false;
	
	if (t == NULL) return NOT_POSSIBLE;
	else {
		// Stop the timer
		stop_timer(t);
		
		// Free the timer(s)
		if (T[t->timers_used[0]].four_B) {
			T[t->timers_used[1]].in_use = false;
			rm1 = true;
		}
		T[t->timers_used[0]].in_use = false;
		
		// Disable interrupts and clock if another one not in use
		for (i = 0; i < TOTAL_TIMERS_AVAILABLE; i++) {
			if (T[i].in_use) {
				if (t_find_chan_num(i) == t_find_chan_num(t->timers_used[0])) rm0 = false;
				else if (t_find_chan_num(i) == t_find_chan_num(t->timers_used[1])) rm1 = false;
			}
		}
		if (rm0) t_enable_disable_clock(t->timers_used[0], false, true);
		if (rm1) t_enable_disable_clock(t->timers_used[1], false, true);
		
		// Remove timer itself
		t_pop_timer(t);
		
		return NO_ERROR;
	}
}

void t_pop_timer(TU *t) {
	TU *temp = timer, *temp2;
	
	if (timer == NULL) return;
	
	if (timer == t) timer = timer->next;
	else {
		for(; temp->next != t && temp->next != NULL; temp = temp->next);
		if (temp->next == NULL) return;
		temp2 = temp;
		temp = temp->next;
		temp2->next = temp->next;
	}
	
	free(temp);
}

void Timer_Handler(uint16_t chan) {
	uint16_t c;
	TU *temp_t;
	
	for (c = chan; c < TOTAL_TIMERS_AVAILABLE; c += CHANNELS_PER_TIMER) {
		if (T[c].in_use) {
			if (TC_SR_CPCS & tc_get_status(t_find_timer_num(c),t_find_chan_num(c))) {
				if (NULL != (temp_t = timer)) {
					do {
						if (c == temp_t->timers_used[0]) {
							if (!temp_t->continuous) stop_timer(temp_t);
							temp_t->fn();
						}
					} while(NULL != (temp_t = temp_t->next));
				}
			}
		}
	}
}

void TC0_Handler() {
	Timer_Handler(0);
}

void TC1_Handler() {
	Timer_Handler(1);
}

void TC2_Handler() {
	Timer_Handler(2);
}
