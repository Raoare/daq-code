#include "sampling.h"

//Data buffer variables
uint32_t bufLen = 0;
TU *s_timer = NULL;
bool timer_done = false;

//Status variable for the state of the system (sampling or not)
startS ss = STOP;

//Corruption variables due to data not being read out fast enough
bool corrupt_sample_set = false;
long corruption_amount = 0;

void samp_timer_handler() {
    cpu_irq_enter_critical();
    if (bufLen > (DEVICE_DATA_BUFFER_SIZE - ADC_NUM_CHANNELS)) {
        //Set data corrupt flag
        corrupt_sample_set = true;
        corruption_amount += ADC_NUM_CHANNELS;
    }
    else adcValBuffer = &(deviceDataResponse.data[(bufLen += ADC_NUM_CHANNELS)]);
    status_check();
    cpu_irq_leave_critical();
}

/******************************************************************
 *
 * Description: Initializes all variables for sampline sets
 * Last Modified: 11/1/17
 *
 ******************************************************************/
void sampling_init(void) {
	bufLen = 0;
}

/******************************************************************
 *
 * Description: Returns the length of the data buffer
 * Last Modified: 11/1/17
 *
 ******************************************************************/
uint16_t get_buf_len(void) {
	return bufLen;
}

/******************************************************************
 *
 * Description: Checks to see if sampling is continuing, complete,
 *  or if another sampling set exists to execute
 * Last Modified: 11/1/17
 *
 ******************************************************************/
void status_check(void) {
	uint8_t temp, s[2] = {STOP_ADC,START_ADC};
	
    if ((temp = dec()) == NULL) stop();
    else if (temp == 2) {
		setRate(queue->rate);
		change_channel(queue->channels);
		txrx(s, NULL, 2, false);
	}
}

/******************************************************************
 *
 * Description: Starts sampling routine
 * Last Modified: 11/1/17
 *
 ******************************************************************/
startS start(void) {
	uint8_t i;
	
    if (ss == STOP && queue != NULL) {
		//change_channel(queue->channels);
        setRate(queue->rate);
        timer_done = false;
        dataRdy = false;
		bufLen = 0;
        return START;
    }
    else return ss;
}

/******************************************************************
 *
 * Description: Stops sampling routine
 * Last Modified: 11/1/17
 *
 ******************************************************************/
startS stop(void) {
	interruptEnable(false);
    return ss = STOP;
}

/******************************************************************
 *
 * Description: Sets the sampling rate for both the ADC and microcontroller
 * Last Modified: 11/1/17
 *
 ******************************************************************/
void setRate(float rate) {
    changeSampleRate(determineADCRate(rate));
    
    // Timer setup
    if (s_timer != NULL) {
        rm_timer(s_timer);
        s_timer = NULL;
    }
    s_timer = setup_timer(false, rate, &samp_timer_handler);
    start_timer(s_timer);
    
    interruptEnable(true);
}

/******************************************************************
 *
 * Description: Enables the interrupts as needed
 * Last Modified: 11/1/17
 *
 ******************************************************************/
void interruptEnable(bool en) {
	startADC(en);
	enableDrdy(en);
    if(en) ss = GO;
    else {
        ss = STOP;
		//TODO: disable timer here
		stop_timer(s_timer);
    }
}

/******************************************************************
 *
 * Description: Gets data ready to send over USB
 * Last Modified: 11/1/17
 *
 ******************************************************************/
uint32_t send_ADC_data(uint16_t numBytes) {
	uint32_t i;
	
	if (bufLen < ADC_NUM_CHANNELS || numBytes < bufLen) return 0;
    //TODO: handle data corruption
    if (corrupt_sample_set) return 0;
    
    //if (timer_done && dataRdy) readData();
    i = bufLen;
    
    // Reset buffer
    adcValBuffer = &(deviceDataResponse.data);
    bufLen = 0;
    
	return sizeof(float)*i;
}

bool is_corrupt(void) {
    return corrupt_sample_set;
}
